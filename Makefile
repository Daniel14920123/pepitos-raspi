KERNEL := pEpitOS
CC = ./arm-toolchain/bin/aarch64-none-elf-gcc
LINKER = ./arm-toolchain/bin/aarch64-none-elf-ld
ASM = ./arm-toolchain/bin/aarch64-none-elf-gcc
OBJCOPY = ./arm-toolchain/bin/aarch64-none-elf-objcopy
CFLAGS = -Wall -Wextra -O0 -Iinclude -g
ASMPARAM = -f elf64 -F dwarf

INTERNALLDFLAGS :=     \
	-nostdlib      \
	-Tlink.ld    \
	-z max-page-size=0x1000
	#-Wl,-static,-pie,--no-dynamic-linker,-ztext -fno-pic -fpie 

INTERNALCFLAGS  :=       \
	-std=c17             \
	-ffreestanding       \
	-fno-stack-protector \
	-fno-pic -fno-pie    \
	-fno-zero-initialized-in-bss \
	-fno-isolate-erroneous-paths-attribute \
	-fno-delete-null-pointer-checks

CFILES := $(shell find ./ -type f -name '*.c')
SFILES := $(shell find ./ -type f -name '*.S')
OBJ    := $(SFILES:.S=.o)
OBJ    += $(CFILES:.c=.o)

.PHONY: all clean

kernel8.img: $(KERNEL).elf
	$(OBJCOPY) -O binary $< $@

$(KERNEL).elf: $(OBJ)
	$(LINKER) $(INTERNALLDFLAGS) $(OBJ) -o $@

%.o: %.c
	$(CC) -o $@ $(CFLAGS) $(INTERNALCFLAGS) -c $<

%.o: %.s
	$(ASM) $(ASMPARAM) -o $@ $<

clean:
	@rm -rf $(KERNEL).elf $(OBJ) image.hdd $(KERNEL).iso limine log.txt iso_root kernel8.img
