#ifndef SERIAL_H
#define SERIAL_H

void uart_init();
void uart_writeText(const char *buffer);

#endif