#ifndef LOG_H
#define LOG_H
void set_log_function(void (*log_func)(const char *));
void log_str(const char *to_log);
void log_long(const long to_log);

#ifdef __cplusplus
template <typename T>
void log(const T to_log)
{
    log_long((long)to_log);
}
void log(const char *to_log);
#else

#define log(x) _Generic((x), char *:log_str((const char*)(x)), const char *:log_str((const char*)x), default:log_long((long)x))

#endif

#define LOG(x)               \
    {                         \
        log((const char *)0); \
        log(__FILE__);        \
        log(" : ");           \
        log(__func__);        \
        log(" l. ");          \
        log(__LINE__);        \
        log(" -> ");          \
        log(x);               \
    }
#define LOGERR(x)               \
    {                         \
        log((const char *)1); \
        log(__FILE__);        \
        log(" : ");           \
        log(__func__);        \
        log(" l. ");          \
        log(__LINE__);        \
        log(" -> ");          \
        log(x);               \
    }
#define LOGOK(x)               \
    {                         \
        log((const char *)2); \
        log(__FILE__);        \
        log(" : ");           \
        log(__func__);        \
        log(" l. ");          \
        log(__LINE__);        \
        log(" -> ");          \
        log(x);               \
    }

#endif