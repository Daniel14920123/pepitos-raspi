#include "log.h"
#define LOG_VEC_SIZE 1
static void(*logger[LOG_VEC_SIZE])(const char*);

void logger_wrapper(char* str){
    if(str[0] == '\n' && str[1] == 0)
        log_str("\n");
    log_str((char*)0);
}

void set_log_function(void(*log_func)(const char*)){
    static unsigned char called_times = 0;
    if(called_times >= LOG_VEC_SIZE)
        return;
    logger[called_times] = log_func;
    called_times++;
}

void log_str(const char* to_log){
    for(unsigned char i = 0; i <LOG_VEC_SIZE && logger[i]; i++)
        logger[i](to_log);
}

void log_long(const long to_log){
    char *dec(long x, char *s);
    char buff[20];
    log_str(dec(to_log, buff+19));
}

#ifdef __cplusplus
void log(const char* to_log){
    log_str(to_log);
}
#endif