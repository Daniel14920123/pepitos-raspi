#include "serial.h"
#include "log.h"

void kmain(){
    uart_init();
    set_log_function(&uart_writeText);
    LOGOK("Booted successfully.");
    LOG("Ceci est un log qualitatif.");
    LOGERR("TOTO.");
    while (1);
}