char *dec(long x, char *s) {
    *--s = 0;
    if (!x) *--s = '0';
    char is_n = x < 0;
    for (; x; x/=10) *--s = '0'+x%10;
    if(is_n) *--s = '-';
    return s;
}